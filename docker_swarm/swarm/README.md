# Docker Swarm
Un swarm és un cluster de hosts que treballen conjuntament. 

#### Creació de swarm (manger)
```
docker swarm init

docker info
docker node ls
```
#### Creació swarm join-token
```
docker swarm join-token worker
docker swarm join-token manager
```
Utilitzem el token generat en el worker per a que formi part del swarm.
#### Desplegació de servei
Manager
```
docker service create --replicas 1 --name helloworld alpine ping docker.com

docker service ls
docker service ps helloworld
```
#### Inspeccionar el servei
Manager
```
docker service inspect --pretty helloworld
docker service inspect helloworld

docker service ps helloworld
```
Al node on s'executa el servei, el podem observar amb la següent ordre:
```
docker ps
```
#### Canviar el "scale"
Si volem executar més rèpliques d'un servei, ho podem fer amb l'ordre:
```
docker service scale helloworld=5

docker service ps helloworld
```
Aquestes rèpliques es desplegaran en els nodes que formen part del swarm.

#### Esborrar serveis
```
docker service rm helloworld
```
