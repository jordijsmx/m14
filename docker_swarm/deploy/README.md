# Deploy

## Desplegament de serveis amb docker swarm / stack

#### Inicialització del swarm
```
docker swarm init

docker swarm join-token worker
```
#### Llistar el node
```
docker node ls

ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
oghp73g1f1f9ajffdmlot98l5     i07        Ready     Active                          24.0.2
ufrv9d800is3f8qfv4l31gtii *   i08        Ready     Active         Leader           24.0.2
webkm9eys0j8hlrvj0tcs5xye     i09        Ready     Active                          24.0.6
```
#### Desplegament de l'stack
Desplegarem l'aplicació del comptador amb 2 rèpliques de l'aplicació web.
```
docker stack deploy -c compose.yml jordiapp
```
#### Llistat dels serveis de l'stack
```
docker service ls

ID             NAME                  MODE         REPLICAS   IMAGE                             PORTS
luuyc3fo9rjn   jordiapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
v97d732g4iat   jordiapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
sty4331un0lr   jordiapp_web          replicated   2/2        edtasixm05/getstarted:comptador   *:80->80/tcp
```
#### Llistat dels processos del servei web
```
docker service ps jordiapp_web

ID             NAME             IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
cxelirlo35nv   jordiapp_web.1   edtasixm05/getstarted:comptador   i07       Running         Running 22 minutes ago             
ljhetnkuweyc   jordiapp_web.2   edtasixm05/getstarted:comptador   i08       Running         Running 22 minutes ago   
```
#### Augmentem les rèpliques de 2 a 5
Ho podem fer editant el fitxer compose.yml o utilitzar la següent ordre
```
docker service scale jordiapp_web=5

docker service ps jordiapp_web

ID             NAME             IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
cxelirlo35nv   jordiapp_web.1   edtasixm05/getstarted:comptador   i07       Running         Running 24 minutes ago             
ljhetnkuweyc   jordiapp_web.2   edtasixm05/getstarted:comptador   i08       Running         Running 24 minutes ago             
nmah35z6om6f   jordiapp_web.3   edtasixm05/getstarted:comptador   i09       Running         Running 19 seconds ago             
sh3idcytfxhh   jordiapp_web.4   edtasixm05/getstarted:comptador   i09       Running         Running 19 seconds ago             
dbz8kzvaczrx   jordiapp_web.5   edtasixm05/getstarted:comptador   i07       Running         Running 20 seconds ago 
```
Al llistar els processos del servei **jordiapp_web** podem observar que les rèpliques es reparteixen entre els nodes worker.

Podem fer el mateix modificant el fitxer compose.yml i canviant la línea de **replicas**:
```
version: "3"
services:
  web:
    image: edtasixm05/getstarted:comptador 
    deploy:
      replicas: 2
      restart_policy:
        condition: on-failure
      resources:
        limits:
          cpus: "0.1"
          memory: 50M
    ports:
      - "80:80"
    networks:
      - webnet
  redis:
    image: redis
    ports:
      - "6379:6379"
    deploy:
      placement:
        constraints: [node.role == manager]
    command: redis-server --appendonly yes
    networks:
      - webnet
  visualizer:
    image: dockersamples/visualizer:stable
    ports:
      - "8080:8080"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    deploy:
      placement:
        constraints: [node.role == manager]
    networks:
      - webnet
networks:
  webnet:
```
#### Esborrar stack
```
docker stack rm jordiapp
```
#### Comprovació
Podem comprovar el funcionament de la web introduïnt al navegador **localhost:80**
```
Hello World!
Hostname: b3bf7a8a8875
Visits: 1
```
Si actualitzem la pàgina, veurem que el comptador augmenta i que el **hostname canvia**:
```
Hello World!
Hostname: 00435bc96197
Visits: 2
```
