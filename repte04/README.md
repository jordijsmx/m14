# Servidor LDAP en detach

### Fitxers necessaris al directori context
```
Dockerfile
edt-org.ldif
slapd.conf
startup.sh
```
### Contingut Dockerfile
En aquest cas hem de substituir CMD per ENTRYPOINT.

- CMD indica l'ordre amb que s'ha diniciar el container. Si s'indica al final del docker run, aquesta ordre té prioritat sobre el CMD indicat.

- Amb ENTRYPOINT la ordre indicada s'executara si o si com a ordre d'arrancada del container (PID 1)
```bash
FROM debian:latest
LABEL author="@jordijsmx Jordi Jubete"
LABEL subject="ldapserver"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
ENTRYPOINT ["/opt/docker/startup.sh"]
EXPOSE 389
```
### Contingut startup.sh
```bash
#!/bin/bash

function initdb() {
  rm -rf /etc/ldap/slapd.d/*
  rm -rf /var/lib/ldap/*
  slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
  slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
  chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
  /usr/sbin/slapd -d0
}

function startdb() {
  /usr/sbin/slapd -d0
}

case $1 in
  initdb)
    echo "Inicialitzant la base de dades" 
    initdb;;
  startdb)
    echo "Engegant la base de dades" 
    startdb;;
esac
```
### Creació de volums
```
docker volume create ldap-config
docker volume create ldap-data
```
### Generar la imatge
```
docker build -t jordijsmx/ldap:repte4 .
```
### Inicialitzar base de dades LDAP (entrypoint "initdb")
```
docker run --rm --name repte4 -h ldap.edt.org -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d jordijsmx/ldap:repte4 initdb
```
### Entrar al container engegat
```
docker exec -it repte4 /bin/bash
```
### Consultes a dins del container
```
slapcat
slapcat | grep dn
slapcat | grep cn
slapcat | grep mail
```
### Consultes a fora del container
```
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘dc=edt,dc=org’
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘dc=edt,dc=org’ dn # Mostra només dn
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘dc=edt,dc=org’ cn mail # Mostra common names i mails
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘ou=usuaris,dc=edt,dc=org’ # Mostra tot a partir d’usuaris
```
### Esborrar entrades
Hem d'indicar l'usuari administrador i la seva contrasenya.
```
docker exec -it repte4 ldapdelete -vx -H ldap://172.17.0.2 -D 'cn=Manager,dc=edt,dc=org' -w secret 'cn=Pau Pou,ou=usuaris,dc=edt,dc=org'
```
### Arrancar la imatge amb persistència de dades (entrypoint "startdb")
- Fitxers de configuració: **/etc/ldap/slapd.d**
- Fitxers de dades: **/var/lib/ldap**
```
docker run --rm --name repte4 -h ldap.edt.org -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d jordijsmx/ldap:repte4 startdb
```
