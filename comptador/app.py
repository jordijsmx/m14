import time

import redis
from flask import Flask

app = Flask(__name__)     # assignem una aplicació a una variable
cache = redis.Redis(host='redis', port=6379) # variable per contactar amb redis

def get_hit_count():
    retries = 5  # nombre de vegades que s'intentara la connexio amb el redis
    while True:
        try:  # prova de contactar amb el redis
            return cache.incr('hits')  # retorna el valor de hits + 1 pel .incr
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)



