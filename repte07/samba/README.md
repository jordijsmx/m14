# Servidor SAMBA

Servidor SAMBA stand alone

## Contingut directori de context
```
Dockerfile  
smb.conf  
startup.sh
```
## Contingut Dockerfile
```
FROM debian:latest
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install procps samba samba-client cifs-utils
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```
## Contingut startup.sh
```
#! /bin/bash

mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.
mkdir /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat
cp /opt/docker/*.md /var/lib/samba/privat/.
cp /opt/docker/smb.conf /etc/samba/smb.conf

useradd -m -s /bin/bash jordi
echo -e "jordi\njordi" | smbpasswd -a jordi

/usr/sbin/smbd
/usr/sbin/nmbd -F
```
## Contingut fitxer de configuració smb.conf
```
[public]
comment = Share de contingut public
path = /var/lib/samba/public
public = yes
browseable = yes
writable = yes
printable = no
guest ok = yes
```
## Generar imatge
```
docker build -t jordijsmx/samba:repte7 .
```
## Arrancar imatge
```
docker run --rm --name samba_repte7 -p 139:139 -p 445:445 -d jordijsmx/samba:repte7
```
## Comprovació
```
smbclient //172.17.0.2/public
smblicnet //localhost/public
```

