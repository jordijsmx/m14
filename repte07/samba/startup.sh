#! /bin/bash

mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.
mkdir /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat
cp /opt/docker/*.md /var/lib/samba/privat/.
cp /opt/docker/smb.conf /etc/samba/smb.conf

useradd -m -s /bin/bash jordi
echo -e "jordi\njordi" | smbpasswd -a jordi

/usr/sbin/smbd
/usr/sbin/nmbd -F
