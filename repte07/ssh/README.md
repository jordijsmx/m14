# SSH Server
Servidor SSH en detach

## Contingut directori de context
```
Dockerfile  
README.md  
useradd.sh
```
## Contingut Dockerfile
```
FROM debian
RUN apt-get update && apt-get -y install openssh-client openssh-server
COPY useradd.sh /tmp
RUN bash /tmp/useradd.sh
WORKDIR /tmp
CMD /etc/init.d/ssh start -D
EXPOSE 22
```
## Contingut useradd.sh
```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
	useradd -m $user
	echo $user:$user | chpasswd
done
```
## Generar imatge
```
docker build -t jordijsmx/ssh:repte7 .
```
## Arrancar imatge en detach
```
docker run --rm --name ssh_repte7 -p 22 -d jordijsmx/ssh:repte7
```
