# Servidor Net

Servidor Net amb els serveis de xarxa **echo** **daytime** **chargen** **ftp** **tftp** i **http**

**Contingut directori de context**
```
chargen-stream  
daytime-stream  
Dockerfile  
echo-stream  
index.html  
README.md  
startup.sh
useradd.sh  
vsftpd.conf
```
**Contingut Dockerfile**
```
FROM debian
RUN apt-get update && apt-get -y install xinetd iproute2 iputils-ping nmap procps net-tools apache2 tftp vsftpd ftp telnet wget vim tftpd-hpa 
COPY daytime-stream chargen-stream echo-stream /etc/xinetd.d/
COPY index.html /var/www/html/
RUN mkdir /opt/docker
COPY * /opt/docker
COPY vsftpd.conf /etc/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE  7 13 19 20 21 69 80
```
**Contingut startup.sh**
```
#!/bin/bash

service vsftpd start
apachectl start
service tftpd-hpa start
/usr/sbin/xinetd -dontfork
```
**Contingut useradd.sh**

En aquest fitxer afegim els usuaris locals, la seva contrasenya i els afegirem al grup de FTP.
```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
	useradd -m -s /bin/bash $user
	echo $user:$user | chpasswd
	usermod -aG ftp $user
done
```
**Fitxer vsftpd.conf**

Al fitxer de configuració del servidor FTP afegim les següents linees que permetràn que usuaris locals es puguin connectar amb la seva contrasenya i tinguin permisos:
```
local_enable=YES
write_enable=YES
local_umask=022
ftpd_banner=Benvingut al servidor FTP!!!!!!
local_root=/srv/ftp/
```
**Generar imatge**
```
docker build -t jordijsmx/net:repte7 .
```
**Arrancar imatge en detach**
```
docker run --rm --name net -p 20:20 -p 21:21 -p 69:69 -d jordijsmx/net:repte7
```
**Entrar a la imatge**
```
docker exec -it net /bin/bash
```
**Comprovació echo (7) daytime (13) i chargen (19)**
```
telnet 172.17.0.2 7
telnet 172.17.0.2 13
telnet 172.17.0.2 19
```

**Comprovació ftp**
```
ftp 172.17.0.2

Connected to 172.17.0.2.
220 Benvingut al servidor FTP!!!!!!
Name (172.17.0.2:jordi): anonymous
331 Please specify the password.
Password: 
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
```
**Comprovació tftp**
```
tftp 172.17.0.2
```
