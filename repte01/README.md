# Repte 1 Sistema operatiu personalitzat

Generar una imatge personalitzada del sistema operatiu Debian amb els paquets
necessaris per poder inspeccionar el sistema. Usar un Dockerfile per automatitzar la
generació. Ha de permetre realitzar les ordres: ps, ping, ip, nmap, tree, vim com a
mínim.

La imatge s’ha de desar al repositori Dockerhub degudament documentada amb un
README.md de markdown.

Cal que tot el desenvolupament de la imatge sigui pública en un repositori de GitLab.

# Contingut directori context
```
Dockerfile
```
## Contingut Dockerfile
```
FROM debian:latest
LABEL author="jordi jubete"
LABEL subject="repte 1"
RUN apt-get update
RUN apt-get -y install procps iproute2 tree nmap vim iputils-ping
```
## Creació imatge
```
docker build -t jordijsmx/debian:repte1 .
```
## Arrancar imatge
```
docker run --rm --name repte1 -h debian -it jordijsmx/debian:repte1
```
