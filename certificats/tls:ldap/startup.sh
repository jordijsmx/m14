#!/bin/bash

cp  /opt/docker/ldap.conf /etc/ldap/ldap.conf
mkdir -p /etc/ssl/certs
mkdir -p /etc/ssl/private
cp /opt/docker/cacert.pem /etc/ssl//certs/
cp /opt/docker/servercert.pem /etc/ssl/certs/
cp /opt/docker/serverkey.pem  /etc/ssl/private/

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif  
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap /etc/ssl/private /etc/ssl/certs

/usr/sbin/slapd -d0 -h 'ldap:/// ldaps:/// ldapi:///'


