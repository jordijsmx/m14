# Practica 01:ldaps

## Desplegament
```
docker build -t jordijsmx/tls:ldap .

docker run --rm --name ldap.edt.org -h ldap.edt.org -d jordijsmx/tls:ldap
```
## Primer:Crear
### Crear una entitat certificadora Veritat Absoluta
- Generem un certificat auto signat i la seva clau
```
openssl req -new -x509 -days 365 -out cacert.pem -keyout cakey.pem
```
```
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:cat
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Veritat Absoluta
Organizational Unit Name (eg, section) []:veritat
Common Name (e.g. server FQDN or YOUR name) []:Veritat Absoluta
Email Address []:veritat@edt.org
```
### Generar un certificat de servidor a nom de ldap.edt.org
- Primer generem la clau privada
```
openssl genrsa -out serverkey.pem 2048
```
- Segon generem un request a partir de la clau privada per signar el nou certificat
```
openssl req -new -key serverkey.pem -out request.pem
```
```
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:cat
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
Organizational Unit Name (eg, section) []:inf
Common Name (e.g. server FQDN or YOUR name) []:ldap.edt.org
Email Address []:ldap@edt.org

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:jupiter
An optional company name []:
```
- Tercer generem el certificat signat per Veritat Absoluta, demanarà el passphrase del CA
```
 openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in request.pem -out servercert.pem -CAcreateserial 
```
### Configurar el servidor ldap per actuar com a servidor ldaps
- fitxer slapd.conf **Hem de comentar la linea TLSCipherSuite, si nò, el container no s'engega.**
```
LSCACertificateFile        /etc/openldap/certs/cacert.pem
TLSCertificateFile          /etc/openldap/certs/servercert.pem
TLSCertificateKeyFile       /etc/openldap/certs/serverkey.pem
TLSVerifyClient       never
#TLSCipherSuite HIGH:MEDIUM:LOW:+SSLv2
```
- fitxer ldap.conf
```
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

BASE	dc=edt,dc=org
URI	ldap://localhost

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

#TLS_CACERTDIR /etc/openldap/certs
TLS_CACERT /opt/docker/cacert.pem

# Turning this off breaks GSSAPI used with krb5 when rdns = false
SASL_NOCANON	on
```
- fitxer startup.sh
```
cp  /opt/docker/ldap.conf /etc/ldap/ldap.conf
mkdir -p /etc/ssl/certs
mkdir -p /etc/ssl/private
cp /opt/docker/cacert.pem /etc/ssl//certs/
cp /opt/docker/servercert.pem /etc/ssl/certs/
cp /opt/docker/serverkey.pem  /etc/ssl/private/

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif  
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap /etc/ssl/private /etc/ssl/certs

/usr/sbin/slapd -d0 -h 'ldap:/// ldaps:/// ldapi:///'
```
### Alternative Names
- fitxer amb extensions de noms alternatius ext.alternate.conf
```
basicConstraints=CA:FALSE
extendedKeyUsage=serverAuth
subjectAltName=IP:172.17.0.2,IP:127.0.0.1,email:copy,URI:ldaps://myldap.edt.org
```
- Comprovació
```
openssl x509 -noout -text -in servercert.pem 

  X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            X509v3 Extended Key Usage: 
                TLS Web Server Authentication
            X509v3 Subject Alternative Name: 
                IP Address:172.17.0.2, IP Address:127.0.0.1, email:ldap@edt.org, URI:ldaps://myldap.edt.org
```

