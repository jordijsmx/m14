# Repte 2 Servidor detach amb propagació de port
Generar un servidor web usant Apache que funcioni en detach. Cal fer la propagació
del port del container al port del host amfitrió.

## Contingut directori context
```
Dockerfile  
index.html    
startup.sh
```
## Contingut Dockerfile
```
FROM debian:latest
LABEL author="jordi jubete"
LABEL subject="webserver"
RUN apt-get update
RUN apt-get -y install procps iproute2 nmap tree vim apache2
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 80
```
## Contingut startup.sh
```
#! /bin/bash

cp /opt/docker/index.html /var/www/html/index.html

apachectl -k start -X
```
## Contingut index.html
```
<html>
	<head>
		<title>M14 REPTE 2</title>
	</head>
	<body>
		<h1>Servidor web</h1>
		Aquesta es la web del repte 2 :)
	</body>
</html>
```
## Creació imatge
```
docker build -t jordijsmx/web:repte2 .
```
## Arrancar imatge
```
docker run --rm --name repte2 -p 80:80 -d jordijsmx/web:repte2
```

Per visualitzar la pàgina escrivim "localhost" al navegador.
