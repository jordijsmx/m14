# Servidor LDAP en detach

### Fitxers necessaris al directori context
```
Dockerfile
edt-org.ldif
slapd.conf
startup.sh
```
### Contingut Dockerfile
Afegim les variables "admin" i "pass" que exportarem al container, en cas de que no volguem afegir la opció "-e" al docker run.

Si volem utilitzar les opcions al docker run, no cal que afegim les linies "ENV".
```bash
FROM debian:latest
LABEL author="@jordijsmx Jordi Jubete"
LABEL subject="ldapserver"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
ENV admin=Manager
ENV pass=secret
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```
### Contingut startup.sh
Afegim dues linies amb la ordre sed on fa una cerca i substitució de les paraules "Manager" i "secret", que son les que venen per defecte al fitxer **slapd.conf**.

- "Manager" el substitueix per el valor que contingui la variable "admin"
- "secret" el substitueix pel valor que contingui la variable "pass"
```bash
#!/bin/bash

  rm -rf /etc/ldap/slapd.d/*
  rm -rf /var/lib/ldap/*
  
  sed -i 's/Manager/'$admin'/' /opt/docker/slapd.conf 
  sed -i 's/secret/'$pass'/' /opt/docker/slapd.conf 

  slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
  slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif  
  chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
  /usr/sbin/slapd -d0
```
### Generar la imatge
```
docker build -t jordijsmx/ldap:repte6 .
```
### Engegar el container en detach
Engeguem el container afegint les opcions "-e" per definir les variables i així canviant el nom d'administrador i la seva contrasenya.
```
docker run --rm --name repte6 -e admin=Jordi -e pass=contrasenya -d jordijsmx/ldap:repte6
```
### Entrar al container engegat
```
docker exec -it repte6 /bin/bash
```
