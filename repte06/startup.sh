#!/bin/bash

  rm -rf /etc/ldap/slapd.d/*
  rm -rf /var/lib/ldap/*
  
  
  sed -i 's/Manager/'$admin'/' /opt/docker/slapd.conf 
  sed -i 's/secret/'$pass'/' /opt/docker/slapd.conf 

#  sed -r -i '/rootdn/ s/.*/rootdn "'$admin'"/' /opt/docker/slapd.conf 

  slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
  slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif  
  chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
  /usr/sbin/slapd -d0
