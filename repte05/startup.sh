#!/bin/bash

function initdb() {
  rm -rf /etc/ldap/slapd.d/*
  rm -rf /var/lib/ldap/*
  slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
  slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif  
  chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
  /usr/sbin/slapd -d0 
}

function slapd() {
  rm -rf /etc/ldap/slapd.d/*
  rm -rf /var/lib/ldap/*
  slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
  chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
  /usr/sbin/slapd -d0 
}	

function start() {
  /usr/sbin/slapd -d0
}

case $1 in
  initdb)
    initdb;;
  slapd)
    slapd;;
  start|edtorg)
    start;;
  slapcat)
    if [ $2 -eq 0 -o $2 -eq 1 ];then
      slapcat -n$2
    else
      slapcat
    fi;;
  *)
    start;;
esac
