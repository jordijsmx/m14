# Servidor LDAP en detach

Implementar una imatge de servidor ldap configurable segons el paràmetre rebut. S’executa amb un entrypoint que és un script que actuarà segons li indiqui l’argument rebut:
 - initdb → ho inicialitza tot de nou i fa el populate de edt.org
 - slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades
 - start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents
 - slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada

### Fitxers necessaris al directori context
```:w
Dockerfile
edt-org.ldif
slapd.conf
startup.sh
```
### Contingut Dockerfile
En aquest cas hem de substituir CMD per ENTRYPOINT.

- CMD indica l'ordre amb que s'ha diniciar el container. Si s'indica al final del docker run, aquesta ordre té prioritat sobre el CMD indicat.

- Amb ENTRYPOINT la ordre indicada s'executara si o si com a ordre d'arrancada del container (PID 1)
```bash
FROM debian:latest
LABEL author="@jordijsmx Jordi Jubete"
LABEL subject="ldapserver"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive # Per a que la instal·lació de paquets no sigui interactiva
RUN apt-get -y install procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
ENTRYPOINT ["/opt/docker/startup.sh"]
EXPOSE 389
```
### Contingut startup.sh
```bash
#!/bin/bash

function initdb() {
  rm -rf /etc/ldap/slapd.d/*
  rm -rf /var/lib/ldap/*
  slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
  slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif  
  chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
  /usr/sbin/slapd -d0 
}

function slapd() {
  rm -rf /etc/ldap/slapd.d/*
  rm -rf /var/lib/ldap/*
  slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
  chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
  /usr/sbin/slapd -d0 
}	

function start() {
  /usr/sbin/slapd -d0
}

case $1 in
  initdb)
    initdb;;
  slapd)
    slapd;;
  start|edtorg)
    start;;
  slapcat)
    if [ $2 -eq 0 -o $2 -eq 1 ];then
      slapcat -n$2
    else
      slapcat
    fi;;
  *)
    start;;
esac
```
### Creació volums
```
docker volume create ldap-config
docker volume create ldap-data
```
### Generar la imatge
```
docker build -t jordijsmx/repte5 .
```
### Inicialitzar base de dades LDAP amb les dades (entrypoint "initdb")
```
docker run --rm --name repte5 -h ldap.edt.org -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d jordijsmx/repte5 initdb
```
### Iinicialitzar base de dades LDAP sense les dades (entrypoint "slapd")
```
docker run --rm --name repte5 -h ldap.edt.org -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d jordijsmx/repte5 slapd
```
## Entrar al container engegat
```
docker exec -it repte5 /bin/bash
```
## Consultes desde el container
```
slapcat
slapcat | grep dn
slapcat | grep cn
slapcat | grep mail
```
## Consultes desde fora del container
```
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘dc=edt,dc=org’
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘dc=edt,dc=org’ dn # Mostra només dn
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘dc=edt,dc=org’ cn mail # Mostra common names i mails
ldapsearch -x -LLL -H ldap://172.17.0.2 -b ‘ou=usuaris,dc=edt,dc=org’ # Mostra tot a partir d’usuaris
```
## Esborrar entrades
```
docker exec -it repte4 ldapdelete -vx -H ldap://172.17.0.2 -D 'cn=Manager,dc=edt,dc=org' -w secret 'cn=Pau Pou,ou=usuaris,dc=edt,dc=org'
```
## Arrancar la imatge amb dades persistents (entrypoint "start / edtorg / res")
- Fitxers de configuració: **/etc/ldap/slapd.d**
- Fitxers de dades: **/var/lib/ldap**
```
docker run --rm --name repte5 -h ldap.edt.org -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d jordijsmx/repte5 start
```
## Slapcat de la base de dades indicada (0,1,res)
```
docker run --rm --name repte5 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -it jordijsmx/ldap:base slapcat
```
