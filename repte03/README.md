# Repte 3 Utilització de volums en un servidor Postgres

Usar postgres (la nostra versió) i una base de dades persistent. Usar la imatge
postgres original amb populate i persistència de dades. Exemples de SQL injectat
usant volumes.

## Contingut directori context
```
Dockerfile  trainingv7.sql
```
## Contingut Dockerfile
```
FROM postgres:latest
COPY trainingv7.sql /docker-entrypoint-initdb.d/ 
```
## Generar imatge
```
docker build -t jordijsmx/postgres:repte3 .
```
## Arrancar imatge de postgres sense persistència de dades
```
docker run --rm --name repte3 -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -d jordijsmx/postgres:repte3

docker run -it --rm postgres psql -h 172.17.0.2 -U postgres -d training
```
## Creació del volum
```
docker volume create postgres
```
## Associar volum al container
```
docker run --rm --name repte3 -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v postgres:/var/lib/postgresql/data -d jordijsmx/postgres:repte3
```
## Realitzar consulta
```
docker exec -it repte3 psql -U postgres -d training -c "select * from oficines;"
```

