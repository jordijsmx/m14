# Postgres + adminer

Exemple servidor postgres training amb persistència de dades i adminer

- User: postgres
- Password: passwd
- db: training

**Contingut compose.yml**
```
version: "2"
services:
  db:
    image: jordijsmx/postgres:repte3
    restart: always
    environment:
      POSTGRES_PASSWORD: passwd
      POSTGRES_DB: training
    volumes:
      - "postgres:/var/lib/postgresql/data"
    networks:
      - 2hisx
  adminer:
    image: adminer
    restart: always
    ports: 
      - 8080:8080
    networks:
      - 2hisx
volumes:
  postgres:
networks:
  2hisx:
```
**Desplegament**
```
docker compose up -d
```

**Comprovació**

Introduir al navegador **localhost:8080** per accedir a l'adminer que ens permetrà visualitzar la base de dades training desde el navegador.

**Comprovació de persistència de dades**
```
docker exec -it repte3 psql -U postgres -d training -c "select * from oficines;"

docker exec -it repte3 psql -U postgres -d training -c "DROP TABLE oficines CASCADE;"
```
