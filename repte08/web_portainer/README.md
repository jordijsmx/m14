# Servidor Web + portainer

Exemple de servidor NET i portainer

#### Contingut compose.yml
```
version: "2"
services:
  web:
    image: jordijsmx/web:repte2
    container_name: web
    hostname: web
    ports:
      - "80:80"
    networks:
      - 2hisx
  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - 2hisx
networks:
  2hisx:
```
#### Desplegament
```
docker compose up -d
```
#### Comprovació Web
Introduir al navegador **localhost:80** per visualitzar la pàgina web creada.

#### Comprovació portainer
Introduir al navegador **localhost:9000** per accedir al gestor d'imatges docker portainer i crear l'usuari administrador amb la seva contrasenya nova.
