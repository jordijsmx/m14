# LDAP + phpldapadmin

Exemple de servidor LDAP i phpldapadmin

**Contingut docker-compose.yml**
```
version: "3"
services:
  ldap:
    image: jordijsmx/ldap:acl
    container_name: ldapserver
    hostname: ldapserver
    ports:
      - "389:389"
    networks:
      - mynet
  phpldapadmin:
    image: jordijsmx/phpldapadmin
    container_name: phpldap
    hostname: phpldap
    ports:
      - "80:80"
    networks:
      - mynet
networks:
  mynet:
```
**Desplegament**
```
docker compose up -d
```
**Comprovació**

Introduir al navegador **localhost/phpldapadmin** per accedir al gestor phpldapadmin i després posar el dn de l'usuari amb qui volguem accedir.
