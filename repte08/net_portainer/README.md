# Servidor NET + portainer

Exemple de servidor NET i portainer

**Contingut compose.yml**
```
version: "2"
services:
  net:
    image: jordijsmx/net:repte7
    container_name: net
    hostname: net
    ports:
      - "7:7"
      - "13:13"
      - "19:19"
    networks:
      - 2hisx
  portainer:
    image: portainer/portainer
    ports:
      - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - 2hisx
networks:
  2hisx:
```
**Desplegament**
```
docker compose up -d
```
**Comprovació echo (7) daytime (13) i chargen (19)**
```
telnet 172.17.0.2 7
telnet 172.17.0.2 13
telnet 172.17.0.2 19
```
**Comprovació portainer**

Introduir al navegador **localhost:9000** per accedir al gestor d'imatges docker portainer i crear l'usuari administrador amb la seva contrasenya nova.
