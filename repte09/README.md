# Comptador de visites

Desplegació de docker compose amb un servei web que mostra una pàgina amb un comptador de visites. El codi python es pot editar en "calent".

- **app.py:** codi de python que utilitza flash i redis per mostrar la pàgina amb el comptador.
- **Dockerfile:** fitxer per a crear la imatge del programa.
- **requirements.txt:** nom dels requeriments necessaris per a que el codi python es pugui executar.
	- **flask:** eina de Python pel desenvolupament d'aplicacions web.
	- **redis:** base de dades en memòria utilitzada en emmagatzematge en caché i gestió de cues de missatges. 
- **compose.yaml:** fitxer per al desplegament dels serveis web i redis

#### Contingut directori context
```
app.py  
compose.yaml  
Dockerfile  
requirements.txt
```
#### Contingut app.py
```
import time

import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
```
#### Contingut Dockerfile
```
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]
```
#### Contingut requirements.txt
```
flask
redis
```
#### Contingut compose.yaml
Es realitza un bind-mount del directori actiu de desenvolupament al directori **/code** del container. Es defineix la variable flask per indicar que ha d'actuar en mode desenvolupament, d'aquesta manera recarregant-se cada vegada que es modifica el codi de l'aplicació.
```
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
    environment:
      FLASK_ENV: development
  redis:
    image: "redis:alpine"
```
#### Comprovació
Introduim al navegador **localhost:5000** i veurem el missatge "Hello World" amb el comptador de visites que va augmentant a cada nova visita. Editem el fitxer **app.py** en calent i comprovar que el missatge canvia en "calent".
